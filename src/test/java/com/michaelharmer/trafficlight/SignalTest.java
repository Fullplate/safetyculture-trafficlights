package com.michaelharmer.trafficlight;

import org.junit.Test;

import static com.michaelharmer.trafficlight.Signal.GO;
import static com.michaelharmer.trafficlight.Signal.PREPARE_TO_STOP;
import static com.michaelharmer.trafficlight.Signal.STOP;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SignalTest {

    @Test
    public void nextSignalFromGoShouldBePrepareToStop() throws Exception {
        assertThat(GO.getNextSignal(), is(PREPARE_TO_STOP));
    }

    @Test
    public void nextSignalFromPrepareToStopShouldBeStop() throws Exception {
        assertThat(PREPARE_TO_STOP.getNextSignal(), is(STOP));
    }

    @Test
    public void nextSignalFromStopShouldBeGo() throws Exception {
        assertThat(STOP.getNextSignal(), is(GO));
    }
}