package com.michaelharmer.trafficlight;

import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static com.michaelharmer.trafficlight.Signal.GO;
import static com.michaelharmer.trafficlight.Signal.PREPARE_TO_STOP;
import static com.michaelharmer.trafficlight.Signal.STOP;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class TrafficLightSystemTest {

    @Test
    public void allowTrafficForTenMinutesShouldCycleSignalsToOriginalState() throws Exception {
        TrafficLightSystem trafficLightSystem = new TrafficLightSystem();
        Map<TrafficLightPair, LinkedHashMap<Long, Signal>> log = trafficLightSystem.allowTrafficFor(10 * 60L);

        assertThat(log.size(), is(2));
        log.forEach((trafficLightPair, signalChanges) -> {
                assertThat(signalChanges.size(), is(3));
                Signal firstChange = signalChanges.entrySet().iterator().next().getValue();
                if (PREPARE_TO_STOP.equals(firstChange)) {
                    assertThat(signalChanges.keySet(), contains(270L, 300L, 600L));
                    assertThat(signalChanges.values(), contains(PREPARE_TO_STOP, STOP, GO));
                } else if (GO.equals(firstChange)) {
                    assertThat(signalChanges.keySet(), contains(300L, 570L, 600L));
                    assertThat(signalChanges.values(), contains(GO, PREPARE_TO_STOP, STOP));
                } else {
                    fail("Unexpected signal change");
                }
        });
    }
}