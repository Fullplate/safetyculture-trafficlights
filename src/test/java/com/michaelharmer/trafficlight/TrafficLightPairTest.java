package com.michaelharmer.trafficlight;

import org.junit.Test;

import java.util.Optional;

import static com.michaelharmer.trafficlight.Signal.GO;
import static com.michaelharmer.trafficlight.Signal.PREPARE_TO_STOP;
import static com.michaelharmer.trafficlight.Signal.STOP;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class TrafficLightPairTest {

    @Test
    public void tickClockAndDoNothingIfSignalDurationHasNotElapsed() throws Exception {
        TrafficLightPair trafficLightPair = new TrafficLightPair("", GO);

        tickClockAndAssertSignalNotChanged(trafficLightPair, GO.getDuration() - 2);
    }

    @Test
    public void tickClockAndChangeSignalFromGoToPrepareToStopIfSignalDurationHasElapsed() throws Exception {
        TrafficLightPair trafficLightPair = new TrafficLightPair("", GO);

        tickClockAndAssertSignalChanged(trafficLightPair, GO.getDuration() - 1, PREPARE_TO_STOP);
    }

    @Test
    public void tickClockAndChangeSignalFromPrepareToStopToStopIfSignalDurationHasElapsed() throws Exception {
        TrafficLightPair trafficLightPair = new TrafficLightPair("", PREPARE_TO_STOP);

        tickClockAndAssertSignalChanged(trafficLightPair, PREPARE_TO_STOP.getDuration() - 1, STOP);
    }

    @Test
    public void tickClockAndChangeSignalFromStopToGoIfSignalDurationHasElapsed() throws Exception {
        TrafficLightPair trafficLightPair = new TrafficLightPair("", STOP);

        tickClockAndAssertSignalChanged(trafficLightPair, STOP.getDuration() - 1, GO);
    }

    private void tickClockAndAssertSignalNotChanged(TrafficLightPair trafficLightPair, Long elapsedTimeSinceChange) {
        Signal signalBefore = trafficLightPair.getSignal();
        trafficLightPair.setElapsedTimeSinceChange(elapsedTimeSinceChange);
        Optional<Signal> reportedChange = trafficLightPair.tick();

        assertThat(reportedChange.isPresent(), is(false));
        assertThat(trafficLightPair.getSignal(), is(signalBefore));
    }

    private void tickClockAndAssertSignalChanged(TrafficLightPair trafficLightPair, Long elapsedTimeSinceChange,
                                                 Signal expectedSignal) {
        trafficLightPair.setElapsedTimeSinceChange(elapsedTimeSinceChange);
        Optional<Signal> reportedChange = trafficLightPair.tick();

        assertThat(reportedChange.isPresent(), is(true));
        assertThat(reportedChange.get(), is(expectedSignal));
        assertThat(trafficLightPair.getSignal(), is(expectedSignal));
    }
}