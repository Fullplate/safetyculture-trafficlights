package com.michaelharmer.trafficlight;

import com.google.common.collect.ImmutableSet;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.google.common.collect.Lists.newArrayList;

/**
 * A TrafficLightSystem represents a set of logically related traffic lights.
 */
public class TrafficLightSystem {

    private Set<TrafficLightPair> trafficLightPairs;

    public TrafficLightSystem() {
        this.trafficLightPairs = ImmutableSet.<TrafficLightPair>builder()
                .add(new TrafficLightPair("(N, S)", Signal.GO))
                .add(new TrafficLightPair("(E, W)", Signal.STOP))
                .build();
    }

    /**
     * 'Turn on' the TrafficLightSystem and allow traffic through for given duration in seconds.
     *
     * Returns a log of timestamps to signal changes, mapped by TrafficLightPair and ordered by time oldest-newest.
     */
    public Map<TrafficLightPair, LinkedHashMap<Long, Signal>> allowTrafficFor(Long durationSeconds) {
        if (durationSeconds <= 0) {
            throw new IllegalArgumentException("duration must be > 0 seconds");
        }

        Map<TrafficLightPair, LinkedHashMap<Long, Signal>> log = new HashMap<>();
        trafficLightPairs.forEach(trafficLightPair -> log.put(trafficLightPair, new LinkedHashMap<>()));

        for (int i = 0; i < durationSeconds; i++) {
            final Long timestamp = (long) (i + 1);
            for (TrafficLightPair trafficLightPair : trafficLightPairs) {
                trafficLightPair.tick()
                        .ifPresent(signalChange -> log.get(trafficLightPair).put(timestamp, signalChange));
            }
        }
        return log;
    }

}
