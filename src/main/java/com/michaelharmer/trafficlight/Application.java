package com.michaelharmer.trafficlight;

import java.util.LinkedHashMap;
import java.util.Map;

import static java.lang.String.format;

public class Application {

    public static void main(String[] args) {
        if (args.length != 1) {
            throw new IllegalArgumentException("Expected 1 argument: duration in seconds");
        }

        Long duration = Long.valueOf(args[0]);
        TrafficLightSystem trafficLightSystem = new TrafficLightSystem();
        Map<TrafficLightPair, LinkedHashMap<Long, Signal>> log = trafficLightSystem.allowTrafficFor(duration);

        log.forEach(((trafficLightPair, signalChanges)
                -> signalChanges.forEach((timestamp, signalChange)
                -> System.out.println((format("T%s minutes - %s changed to %s",
                        (double) timestamp/60L, trafficLightPair.toString(), signalChange.toString()))))));
    }

}
