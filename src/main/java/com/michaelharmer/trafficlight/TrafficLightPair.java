package com.michaelharmer.trafficlight;

import com.google.common.annotations.VisibleForTesting;

import java.util.Optional;

/**
 * TrafficLightPair contains a finite state machine describing a traffic light.
 */
public class TrafficLightPair {

    private String name;
    private Signal signal;
    private Long elapsedTimeSinceChange;

    public TrafficLightPair(String name, Signal initialSignal) {
        if (name == null || initialSignal == null) {
            throw new IllegalArgumentException("Null arguments not accepted");
        }

        this.name = name;
        this.signal = initialSignal;
        this.elapsedTimeSinceChange = 0L;
    }

    /**
     * Increment the clock, and optionally return the Signal that was changed to.
     */
    public Optional<Signal> tick() {
        this.elapsedTimeSinceChange++;
        if (this.elapsedTimeSinceChange >= signal.getDuration()) {
            this.signal = this.signal.getNextSignal();
            this.elapsedTimeSinceChange = 0L;
            return Optional.of(this.signal);
        }
        return Optional.empty();
    }

    public Signal getSignal() {
        return signal;
    }

    @VisibleForTesting
    void setElapsedTimeSinceChange(Long elapsedTimeSinceChange) {
        this.elapsedTimeSinceChange = elapsedTimeSinceChange;
    }

    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TrafficLightPair that = (TrafficLightPair) o;

        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
