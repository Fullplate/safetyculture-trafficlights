package com.michaelharmer.trafficlight;

/**
 * Signal is an immutable enum defining the possible states and transitions of a TrafficLightPair.
 */
public enum Signal {
    GO("green", 270L),
    PREPARE_TO_STOP("yellow", 30L),
    STOP("red", 300L);

    private String descriptor;
    private Long duration;
    private Signal nextSignal;

    static {
        GO.nextSignal = PREPARE_TO_STOP;
        PREPARE_TO_STOP.nextSignal = STOP;
        STOP.nextSignal = GO;
    }

    Signal(String descriptor, Long duration) {
        this.descriptor = descriptor;
        this.duration = duration;
    }

    public Long getDuration() {
        return duration;
    }

    public Signal getNextSignal() {
        return nextSignal;
    }

    @Override
    public String toString() {
        return descriptor;
    }
}
