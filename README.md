Compiling and running
--
Run unit tests: ./gradlew test  
Run application, with simulated duration of 30 minutes: ./gradlew runTrafficLights -Pduration=1800

Output for given scenario
--
Note: (N, S) initialized as 'green', (E, W) initialized as 'red'  
T4.5 minutes - (N, S) changed to yellow  
T5.0 minutes - (N, S) changed to red  
T10.0 minutes - (N, S) changed to green  
T14.5 minutes - (N, S) changed to yellow  
T15.0 minutes - (N, S) changed to red  
T20.0 minutes - (N, S) changed to green  
T24.5 minutes - (N, S) changed to yellow  
T25.0 minutes - (N, S) changed to red  
T30.0 minutes - (N, S) changed to green  
T5.0 minutes - (E, W) changed to green  
T9.5 minutes - (E, W) changed to yellow  
T10.0 minutes - (E, W) changed to red  
T15.0 minutes - (E, W) changed to green  
T19.5 minutes - (E, W) changed to yellow  
T20.0 minutes - (E, W) changed to red  
T25.0 minutes - (E, W) changed to green  
T29.5 minutes - (E, W) changed to yellow  
T30.0 minutes - (E, W) changed to red  

Assumptions
--
- The intersection we are modeling has two roads only, each with a corresponding pair of lights  
- Each pair of lights displays the same signal  
- One pair of lights is initialized in a GO (green) state, and the other in a STOP (red) state  
- We do not need to cater for vehicles turning or pedestrian crossings, only N <--> S and E <--> W travel  
- Time resolution is seconds  
- The simulation is only executed once per application run  

Design
--
A TrafficLightSystem contains 2 instances of TrafficLightPair. When "allowTrafficFor(Long duration)" is called,
each TrafficLightPair is "ticked", once per second.  
Each TrafficLightPair is a state machine which may be in one of 3 Signal states at any one time. These states are
GO (green), PREPARE_TO_STOP (yellow) and STOP (red), and each is defined with the duration of time that the state should
be maintained for.  
When TrafficLightPair is ticked, it will check if the full duration of its current state has elapsed, and if so will
transition to the next state and report this change to the TrafficLightSystem.
The TrafficLightSystem will collate all signal changes and return them to the caller. 

Safety
--
The key constraint for ensuring safety is that only one 'TrafficLightPair' should be in state GO at any given time.

In my solution, this is ensured by:   
1) Maintaining a central clock  
2) Ensuring only one TrafficLightPair is initialized with the state GO  
3) Defining a schedule for state changes with an even amount of time in states GO (and PREPARE_TO_STOP) and in state
   STOP.  

Some more robust, production-grade solutions could be:  
- Maintaining a (potentially distributed) lock, and ensuring that only one light can acquire the lock at any given time.  
- Requiring a light to request permission for GO state changes from the other light(s). If the other light is in state
  STOP, permission will be granted. If the other light is in states GO or PREPARE_TO_STOP, permission will be denied.
  If denied, the light could poll until permission was granted.

In a real-world solution, each light could be treated as a node in a distributed system. Any node failure or network
partition would then prevent any lights from transitioning to GO, which is likely desirable from a safety perspective.